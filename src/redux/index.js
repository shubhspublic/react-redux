export { buyCake, restoreCakes } from "../redux/cake/cakeActions";
export {
  buyIceCream,
  restoreIceCream,
} from "../redux/iceCream/iceCreamActions";
export {
  fetchUserRequest,
  fetchUserSuccess,
  fetchUserFailed,
  fetchUsers,
} from "./users/userActions";

// export * from "../redux/user/userActions";
