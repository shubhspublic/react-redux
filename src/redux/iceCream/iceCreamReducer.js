import { BUY_ICECREAM, RESTORE_ICECREAM } from "./iceCreamTypes";

const initialState = {
  numOfIceCreams: 20,
};

const iceCreamReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_ICECREAM:
      return {
        ...state,
        numOfIceCreams: state.numOfIceCreams - parseInt(action.payload),
      };

    case RESTORE_ICECREAM:
      return {
        ...state,
        numOfIceCreams: action.payload,
      };

    default:
      return state;
  }
};

export default iceCreamReducer;
