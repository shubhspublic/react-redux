import { BUY_ICECREAM, RESTORE_ICECREAM } from "./iceCreamTypes";

export const buyIceCream = (iceCreamCount = 1) => {
  return {
    type: BUY_ICECREAM,
    payload:iceCreamCount
  };
};

export const restoreIceCream = (iceCreamCount) => {
  return {
    type: RESTORE_ICECREAM,
    payload: iceCreamCount,
  };
};


/*
 Actions are of 2 types :

 1. Synchronous Actions : As soon as action was dispatched, the state was updated immediately.

 2. Asynchronous Actions : We wait for sometask to complete before dispatching action. 
                              
                           This is to fetch data from an end point and use that in your application.

*/