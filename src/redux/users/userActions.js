import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILED,
} from "./userTypes";

import axios from "axios";

export const fetchUserRequest = () => {
  return {
    type: FETCH_USERS_REQUEST,
  };
};

export const fetchUserSuccess = (user) => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: user,
  };
};

export const fetchUserFailed = (error) => {
  return {
    type: FETCH_USERS_FAILED,
    payload: error,
  };
};

export const fetchUsers = () => {
  //this function does not need to pure as we have used redux-thunk as middleware
  return (dispatch) => {
    console.log("fetchUsers invoked")
    // this return function also gets dispatch method as its argument

    //before making api-call, may be to show loader, dispatch FETCH_USERS_REQUEST
    dispatch(fetchUserRequest());

    //making api-call
    axios
      .get("https://jsonplaceholder.typicode.com/users") // fake-free api
      .then((response) => {
        //here, response.data is an array of users
        const users = response.data//.map((user) => user.id);
        console.log("data from api --> ", users)
        dispatch(fetchUserSuccess(users));
      })
      .catch((error) => {
        //here, error.message gives the description of the error
        dispatch(fetchUserFailed(error.message));
      });
  };
};
