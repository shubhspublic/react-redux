import { createStore, applyMiddleware } from "redux";
import rootReducer from "./rootReducer";
import rootMiddleware from "./rootMiddleware";
import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...rootMiddleware))
);

export default store;
