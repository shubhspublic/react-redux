import { BUY_CAKE, RESTORE_CAKES } from "./cakeTypes";

export const buyCake = () => {
  return {
    type: BUY_CAKE,
  };
};

export const restoreCakes = (cakesCount) => {
  return {
    type: RESTORE_CAKES,
    payload: cakesCount,
  };
};


/*
 Actions are of 2 types :

 1. Synchronous Actions : As soon as action was dispatched, the state was updated immediately.

 2. Asynchronous Actions : We wait for sometask to complete before dispatching action. 
                              
                           This is to fetch data from an end point and use that in your application.

*/
