import logger from "redux-logger"; //--> this will log all actions with state of store
import thunk from "redux-thunk"; // --> redux thunk allows action to be asynchronous i.e. api request(not-pure)

//Also, thunks helps to return function(using which you can do various tasks) instead of action-creator

//Creata a middlewares array, if you have multiple arguments to pass as middlewares
const middlewares = [thunk, logger];

export default middlewares;
