import React, { useState } from "react";
import "./CakeContainer.css";
import { buyIceCream, restoreIceCream } from "../redux";
import { useSelector, useDispatch } from "react-redux";
// 1. this useSelector function receives redux-state as its argument
// i.e. it works as a substitute for mapStateToProps function

// 2. this useDispatch function returns a reference to dispatch function from redux-store
// i.e. it works as a substitute for mapDispatchToProps function

const IceCreamContainer = () => {
  const numOfIceCreams = useSelector((state) => state.iceCream.numOfIceCreams);
  const dispatch = useDispatch();
  const [getIceCream, setGetIceCream] = useState("1");

  const handleBuyIceCreamOnClick = () => {
    if (numOfIceCreams < 1) {
      alert("no more iceCream is available");
    } else {
      if (getIceCream.length < 1) {
        alert("You know that, you didn't inserted a number");
      } else if (parseInt(getIceCream) < 1) {
        alert(
          "You know that, you cannot buy " + numOfIceCreams + " iceCreams"
        );
      } else if (parseInt(getIceCream) > numOfIceCreams) {
        alert(
          "Currently, we are limited to " + numOfIceCreams + " iceCreams only"
        );
      } else {
        dispatch(buyIceCream(getIceCream));
        setGetIceCream("");
      }
    }
  };

  const handleAddIceCreamOnClick = () => {
    if (numOfIceCreams > 0) {
      alert("IceCream has not finished yet");
    } else {
      let newIceCreamCount = prompt(
        "Enter no. of iceCream to be added to store"
      );
      if (newIceCreamCount.length < 1 || parseInt(newIceCreamCount) < 1) {
        alert("Are you stupid ? How can you add " + newIceCreamCount + " or no icecream ?");
      } else {
        dispatch(restoreIceCream(newIceCreamCount));
      }
    }
  };

  return (
    <div className="container">
      <h2>Num of icecreams - {numOfIceCreams}</h2>
      <input
        placeholder="Enter no. of icecreams you want to buy"
        type="number"
        value={getIceCream}
        onChange={(e) => setGetIceCream(e.target.value)}
      />
      <button onClick={handleBuyIceCreamOnClick}>Buy {getIceCream} Icecream</button>
      <button onClick={handleAddIceCreamOnClick}>Add Icecream To Store</button>
    </div>
  );
};

export default IceCreamContainer;
