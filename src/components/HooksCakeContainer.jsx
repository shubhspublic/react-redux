import React from "react";
import "./CakeContainer.css";
import { buyCake, restoreCakes} from "../redux";
import { useSelector, useDispatch } from "react-redux";
// 1. this useSelector function receives redux-state as its argument
// i.e. it works as a substitute for mapStateToProps function

// 2. this useDispatch function returns a reference to dispatch function from redux-store
// i.e. it works as a substitute for mapDispatchToProps function

const HooksCakeContainer = () => {
  const numOfCakes = useSelector((state) => state.cake.numOfCakes);
  const dispatch = useDispatch();

  const handleBuyCakeOnClick = () => {
    if (numOfCakes < 1) {
      alert("no more cake is available");
    } else {
      dispatch(buyCake());
    }
  };

  const handleAddCakesOnClick = () => {
    if (numOfCakes > 0) {
      alert("Cake has not finished yet");
    } else {
      let newCakeCount = prompt("Enter no. of cakes to be added to store");
      dispatch(restoreCakes(newCakeCount));
    }
  };

  return (
    <div className="container">
      <h2>Num of cakes - {numOfCakes}</h2>
      <button onClick={handleBuyCakeOnClick}>Buy cake</button>
      <button onClick={handleAddCakesOnClick}>Add Cakes To Store</button>
    </div>
  );
};

export default HooksCakeContainer;
