import React from "react";
import "./CakeContainer.css";
import { buyCake, restoreCakes } from "../redux";
import { connect } from "react-redux";

const CakeContainer = (props) => {
  const handleBuyCakeOnClick = () => {
    if (parseInt(props.numOfCakes) < 1) {
      alert("no more cake is available");
    } else {
      props.buyCake();
    }
  };

  const handleAddCakesOnClick = () => {
    if (parseInt(props.numOfCakes) < 1) {
      props.restoreCakes();
    } else {
      alert("Cake has not finished yet");
    }
  };

  return (
    <div className="container">
      <h2>Number of cakes - {props.numOfCakes}</h2>
      <button onClick={handleBuyCakeOnClick}>Buy Cake</button>
      <button onClick={handleAddCakesOnClick}>Add Cakes To Store</button>
    </div>
  );
};

// to get redux state in props
const mapStateToProps = (state) => {
  return {
    numOfCakes: state.cake.numOfCakes,
  };
};

//to map action_creator to our props
const mapDispatchToProps = (dispatch) => {
  return {
    buyCake: () => dispatch(buyCake()),
    restoreCakes: () => {
      let newCakeCount = prompt("Enter no. of cakes to be added to store");
      dispatch(restoreCakes(newCakeCount));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer);
