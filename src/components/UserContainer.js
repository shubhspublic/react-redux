import React, { useEffect } from "react";
import "./CakeContainer.css";
import { fetchUsers } from "../redux";
import { useSelector, useDispatch } from "react-redux";

const UserContainer = () => {
  const userData = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  }, []);

  return (
    <div className="container">
      {userData.loading ? (
        <h2>Loading</h2>
      ) : userData.error ? (
        <h2>{userData.error}</h2>
      ) : (
        <div>
          <h2>Users List</h2>
          <div className="userList">
            {userData &&
              userData.user &&
              userData.user.map((user) => <p>{user.name}</p>)}
          </div>
        </div>
      )}
    </div>
  );
};

export default UserContainer;
